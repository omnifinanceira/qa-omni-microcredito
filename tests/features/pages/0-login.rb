require_relative 'Util.rb'

class Login < Util

    set_url '/login'

    element :elementinputusuario, :xpath, "//input[@id='user-login']"
    element :elementinputsenha, :xpath, "//input[@id='user-password']"
    element :elementbuttonconfirmar, :xpath, "//span[text()='Entrar']"

    def preencher_usuario(usuario)
        elementinputusuario.set usuario
    end

    def preencher_senha(senha)
        elementinputsenha.set senha
    end

    def clicar_confirmar
        elementbuttonconfirmar.click
    end

    def logar_lojista(usuario = $usuariolojista, senha = $senhalojista)
        preencher_usuario(usuario)
        preencher_senha(senha)
        clicar_confirmar
    end

    def criar_nova_ficha
        find(:xpath, "//span[text()=' Nova Ficha ']").click
    end

end
